# Translation of kcm_autostart to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2008, 2015, 2018, 2020, 2021, 2022.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009, 2011.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:34+0000\n"
"PO-Revision-Date: 2022-09-22 21:57+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: autostartmodel.cpp:320
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "«%1» er ikkje ei absoluttadresse."

#: autostartmodel.cpp:323
#, kde-format
msgid "\"%1\" does not exist."
msgstr "«%1» finst ikkje."

#: autostartmodel.cpp:326
#, kde-format
msgid "\"%1\" is not a file."
msgstr "«%1» er ikkje ei fil."

#: autostartmodel.cpp:329
#, kde-format
msgid "\"%1\" is not readable."
msgstr "«%1» er ikkje lesbar."

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr "Gjer køyrbar"

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr "Fila «%1» må vera køyrbar for å kunna køyrast ved utlogging."

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr "Fila «%1» må vera køyrbar for å kunna køyrast ved innlogging."

#: ui/main.qml:95
#, kde-format
msgid "Properties"
msgstr "Eigenskapar"

#: ui/main.qml:101
#, kde-format
msgid "Remove"
msgstr "Fjern"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr "Program"

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr "Skript ved innlogging"

#: ui/main.qml:118
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Skript før oppstart"

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr "Skript ved utlogging"

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr "Ingen brukardefinerte autostart-oppføringar"

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""
"Trykk <interface>Legg til …</interface> for å leggja til ei ny oppføring"

#: ui/main.qml:145
#, kde-format
msgid "Choose Login Script"
msgstr "Vel innloggings­skript"

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr "Vel skript for utlogging"

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr "Legg til …"

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr "Legg til program …"

#: ui/main.qml:203
#, kde-format
msgid "Add Login Script…"
msgstr "Legg til skript for innlogging …"

#: ui/main.qml:209
#, kde-format
msgid "Add Logout Script…"
msgstr "Legg til skript for utlogging …"
