# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Thanomsub Noppaburana <donga.nb@gmail.com>, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:34+0000\n"
"PO-Revision-Date: 2010-07-07 15:25+0700\n"
"Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: autostartmodel.cpp:320
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:323
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:326
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:329
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties"
msgid "Properties"
msgstr "คุณ&สมบัติ"

#: ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgid "Remove"
msgstr "เอา&ออก"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:118
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "เริ่มก่อน KDE เริ่มทำงาน"

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "เพิ่มสคริปต์..."

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "เพิ่มสคริปต์..."

#: ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "เพิ่มสคริปต์..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ถนอมทรัพย์ นพบูรณ์"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "donga.nb@gmail.com"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "มอดูลศูนย์ควบคุมของ KDE สำหรับจัดการการเริ่มอัตโนมัติ"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "สงวนลิขสิทธิ์ (c) 2006-2010 กลุ่มผู้พัฒนา Autostart Manager"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "ผู้ดูแล"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "ขั้นสูง"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "สคริปต์ของเชลล์:"

#~ msgid "Create as symlink"
#~ msgstr "สร้างการเชื่อมโยงสัญลักษณ์"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "เริ่มอัตโนมัติใน KDE เท่านั้น"

#~ msgid "Name"
#~ msgstr "ชื่อ"

#~ msgid "Command"
#~ msgstr "คำสั่ง"

#~ msgid "Status"
#~ msgstr "สถานะ"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "ประมวลผลเมื่อ"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "ตัวจัดการการเริ่มอัตโนมัติสำหรับ KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "เปิดใช้งาน"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "ไม่ใช้งาน"

#~ msgid "Desktop File"
#~ msgstr "แฟ้มพื้นที่ทำงาน"

#~ msgid "Script File"
#~ msgstr "แฟ้มสคริปต์"

#~ msgid "Add Program..."
#~ msgstr "เพิ่มโปรแกรม..."

#~ msgid "Startup"
#~ msgstr "เริ่มวาระงาน"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr "อนุญาตให้ใช้แฟ้มที่มีส่วนขยายเป็น \".sh\" เท่านั้นสำหรับใช้ในการตั้งค่าสภาพแวดล้อม"

#~ msgid "Shutdown"
#~ msgstr "ปิดระบบ"

#~ msgid "1"
#~ msgstr "1"
