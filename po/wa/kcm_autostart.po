# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jean Cayron <jean.cayron@gmail.com>, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-01 02:34+0000\n"
"PO-Revision-Date: 2010-10-22 18:25+0200\n"
"Last-Translator: Jean Cayron <jean.cayron@gmail.com>\n"
"Language-Team: Walloon <linux@walon.org>\n"
"Language: wa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:320
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr ""

#: autostartmodel.cpp:323
#, kde-format
msgid "\"%1\" does not exist."
msgstr ""

#: autostartmodel.cpp:326
#, kde-format
msgid "\"%1\" is not a file."
msgstr ""

#: autostartmodel.cpp:329
#, kde-format
msgid "\"%1\" is not readable."
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid "Make Executable"
msgstr ""

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""

#: ui/main.qml:57
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""

#: ui/main.qml:95
#, fuzzy, kde-format
#| msgid "&Properties"
msgid "Properties"
msgstr "&Prôpietés"

#: ui/main.qml:101
#, fuzzy, kde-format
#| msgid "&Remove"
msgid "Remove"
msgstr "&Oister"

#: ui/main.qml:112
#, kde-format
msgid "Applications"
msgstr ""

#: ui/main.qml:115
#, kde-format
msgid "Login Scripts"
msgstr ""

#: ui/main.qml:118
#, fuzzy, kde-format
#| msgid "Pre-KDE startup"
msgid "Pre-startup Scripts"
msgstr "Å pré-enondaedje di KDE"

#: ui/main.qml:121
#, kde-format
msgid "Logout Scripts"
msgstr ""

#: ui/main.qml:130
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:131
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add…</interface> button below to add some"
msgstr ""

#: ui/main.qml:145
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Choose Login Script"
msgstr "Radjouter scripe..."

#: ui/main.qml:165
#, kde-format
msgid "Choose Logout Script"
msgstr ""

#: ui/main.qml:182
#, kde-format
msgid "Add…"
msgstr ""

#: ui/main.qml:197
#, kde-format
msgid "Add Application…"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Login Script…"
msgstr "Radjouter scripe..."

#: ui/main.qml:209
#, fuzzy, kde-format
#| msgid "Add Script..."
msgid "Add Logout Script…"
msgstr "Radjouter scripe..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Djan Cayron"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "jean.cayron@gmail.com"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Module do scriftôr di controle do manaedjeu d' enondaedje tot seu"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Copyright © 2006-2010 Ekipe do manaedjeu d' enondaedje tot seu"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Mintneu"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "Sipepieus"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "Scripe shell:"

#~ msgid "Create as symlink"
#~ msgstr "Ahiver come loyén simbolike"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "N' enonder tot seu ki dins KDE"

#~ msgid "Name"
#~ msgstr "No"

#~ msgid "Command"
#~ msgstr "Comande"

#~ msgid "Status"
#~ msgstr "Estat"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Enonder"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "Manaedjeu d' enondaedje tot seu di KDE"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "En alaedje"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Essocté"

#~ msgid "Desktop File"
#~ msgstr "Fitchî d' sicribanne "

#~ msgid "Script File"
#~ msgstr "Fitchî scripe"

#~ msgid "Add Program..."
#~ msgstr "Radjouter programe..."

#~ msgid "Startup"
#~ msgstr "A l' enondaedje"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "I gn a k' les fitchîs avou des cawetes \".sh\" ki sont permetowes pos "
#~ "apontyî l' evironmint."

#~ msgid "Shutdown"
#~ msgstr "Å distindaedje"

#~ msgid "1"
#~ msgstr "1"
