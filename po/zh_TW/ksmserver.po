# translation of ksmserver.po to Chinese Traditional
#
# Traditional Chinese Translation of ksmserver
# Copyright (C) 2001, 2002, 2007, 2008 Free Software Foundation, Inc.
#
# Jing-Jong Shyue <shyue@sonoma.com.tw>, 2001.
# Yuan-Chen Cheng <ycheng@slat.org>, 2002.
# Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>, 2007, 2008, 2009, 2010.
# Franklin Weng <franklin at goodhorse dot idv dot tw>, 2007, 2008.
# Franklin Weng <franklin@mail.everfocus.com.tw>, 2012, 2014.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018, 2020.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-27 02:13+0000\n"
"PO-Revision-Date: 2023-03-19 09:10+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"dot tw>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.3\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "登出已由 %1 取消"

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr "未設定 $HOME。"

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr "$HOME 目錄（%1）不存在。"

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr "沒有讀取 $HOME 目錄（%1）的權限。"

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr "$HOME 目錄（%1）磁碟空間已用盡。"

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr "寫入 $HOME 目錄（%2）時發生錯誤 '%1'"

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr "沒有寫入 '%1' 的權限。"

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr "沒有讀取 '%1' 的權限。"

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr "暫存目錄（%1）磁碟空間已用盡。"

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr "寫入暫存目錄（%2）時發生錯誤 '%1'"

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr "啟動 Plasma 時偵測到以下的安裝問題："

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""
"\n"
"\n"
"無法啟動 Plasma。\n"

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr "Plasma 工作空間安裝發生問題。"

#: main.cpp:193
#, kde-format
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"可信賴的 Plasma 工作階段管理者應該使用標準的\n"
"X11R6 工作階段管理協定 (XSMP)。"

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "如果可以，回復之前的工作階段"

#: main.cpp:200
#, kde-format
msgid "Also allow remote connections"
msgstr "也允許遠端連接"

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr "以鎖定模式啟動工作階段"

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr "以不支援鎖定螢幕的方式啟動。只在有其他組件提供鎖定螢幕時才需要。"

#: server.cpp:881
#, kde-format
msgid "Session Management"
msgstr "工作階段管理"

#: server.cpp:886
#, kde-format
msgid "Log Out"
msgstr "登出"

#: server.cpp:891
#, kde-format
msgid "Shut Down"
msgstr "關機"

#: server.cpp:896
#, kde-format
msgid "Reboot"
msgstr "重新啟動"

#: server.cpp:902
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "不經確認直接登出"

#: server.cpp:907
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr "不經確認直接關機"

#: server.cpp:912
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "不經確認直接重新啟動"

#~ msgid "No write access to $HOME directory (%1)."
#~ msgstr "沒有寫入 $HOME 目錄（%1）的權限。"

#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "當沒有視窗管理員時，使用 <wm> 做為視窗管理員。\n"
#~ "預設為 'kwin'。"

#~ msgid "wm"
#~ msgstr "wm"
